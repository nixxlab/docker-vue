# VueJS CLI

Docker image based on `node:11-alpine`.


## Development environment

Console: `docker run --rm -ti -p 8000:8000 -p 8080:8080 -v $PWD:/app:rw nixxlab/vue sh`

Create new project: `docker run --rm -ti -v $PWD:/app:rw nixxlab/vue vue create myproject`

Install dependencies: `docker run --rm -ti -v $PWD:/app:rw nixxlab/vue npm install`

Serve: `docker run --rm -ti -v $PWD:/app:rw -p 8080:8080 nixxlab/vue npm run serve`

For sync files you can make `vue.config.js` in the project folder contains:
```
module.exports = {
  configureWebpack: {
    devServer: {
      watchOptions: {
        ignored: /node_modules/,
        poll: 1000
      }
    }
  }
}
```

## Production environment

Build: `docker run --rm -ti -v $PWD:/app:rw nixxlab/vue npm run build`

